package com.example.fabio.neontest.factories;

import com.example.fabio.neontest.ui.main.MainContract;
import com.example.fabio.neontest.ui.main.MainPresenter;
import com.example.fabio.neontest.ui.sendmoney.SendMoneyContract;
import com.example.fabio.neontest.ui.sendmoney.SendMoneyPresenter;

/**
 * Created by EUROCOM on 19/08/2017.
 */

public class SendMoneyPresenterFactory {
    public static SendMoneyContract.Actions SendMoneyPresenter(SendMoneyContract.View view) {
        return new SendMoneyPresenter(view);
    }
}
