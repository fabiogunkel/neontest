package com.example.fabio.neontest.apis;

import com.example.fabio.neontest.models.Contact;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public interface IRequestsApi {

    public Observable<String> getToken(String name, String email);
    public Observable<String> sendMoney(String clientID, String token, double value);
    public Observable<ArrayList<Contact>> getTransfers(String token);
}
