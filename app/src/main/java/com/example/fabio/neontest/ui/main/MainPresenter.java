package com.example.fabio.neontest.ui.main;

import android.util.Log;
import android.widget.Toast;

import com.example.fabio.neontest.NeonTestApplication;
import com.example.fabio.neontest.apis.IContactsApi;
import com.example.fabio.neontest.apis.IRequestsApi;
import com.example.fabio.neontest.factories.ContactFactory;
import com.example.fabio.neontest.factories.RequestsFactory;
import com.example.fabio.neontest.models.Contact;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.internal.schedulers.NewThreadScheduler;
import rx.schedulers.Schedulers;

import static com.example.fabio.neontest.apis.RequestsApi.retrofit;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public class MainPresenter implements MainContract.Actions {

    private MainContract.View view;
    private IContactsApi mContactsApi;
    private IRequestsApi mRequestsApi;
    private boolean      changingScreen;
    public MainPresenter(MainContract.View view) {
        this.view    = view;
        mContactsApi = ContactFactory.getContactApi();
        mRequestsApi = RequestsFactory.getRequestsApi();
        view.showUser(mContactsApi.getMyContact());
        getToken();

    }

    @Override
    public void getToken() {
        Contact contact = mContactsApi.getMyContact();
        mRequestsApi
        .getToken(mContactsApi.getMyContact().getName(), mContactsApi.getMyContact().getEmail())
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
        .unsubscribeOn(Schedulers.io())
        .subscribe(
        new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }
            @Override
            public void onError(Throwable e) {
                view.makeFailureDialogBox();

            }

            @Override
            public void onNext(String s) {
                NeonTestApplication.token = s;
            }
        });

    }
}
