package com.example.fabio.neontest.ui.sendmoney;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fabio.neontest.models.Contact;
import com.example.fabio.neontest.R;
import com.example.fabio.neontest.utils.GenericUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.Serializable;

public class SendMoneyContactFragment extends Fragment {

    private static final String CONTACT_KEY = "contact_key";

    private Target imageTarget;

    Contact contact;

    private OnFragmentInteractionListener mListener;

    public SendMoneyContactFragment() {
        // Required empty public constructor
    }


    public static SendMoneyContactFragment newInstance(Contact contact) {
        SendMoneyContactFragment fragment = new SendMoneyContactFragment();
        Bundle args = new Bundle();
        args.putSerializable(CONTACT_KEY, (Serializable) contact);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            contact = getArguments().getParcelable("Contact");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_send_money_contact, container, false);
        ((TextView) view.findViewById(R.id.tvContactName)).setText(contact.getName());
        ((TextView) view.findViewById(R.id.tvContactNumber)).setText(contact.getNumber());
        final EditText editText = (EditText) view.findViewById(R.id.etValueToSend);
        editText.addTextChangedListener( GenericUtils.valueTextWatcher(editText));
        view.findViewById(R.id.btSendMoneyToContact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SendMoneyContract.View)getActivity()).sendMoneyToContactButtonClicked(contact, editText);
            }
        });

        View.OnClickListener closeFragment = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SendMoneyContract.View)getActivity()).closePaymentOverlay();
            }
        };
        view.findViewById(R.id.btCloseButton).setOnClickListener(closeFragment);
        view.findViewById(R.id.llBackround).setOnClickListener(closeFragment);

        final ImageView contactImage =(ImageView) view.findViewById(R.id.ivContactView);
        final int imageWidth = contactImage.getLayoutParams().width;
        final int imageHeight = contactImage.getLayoutParams().height;
        imageTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Bitmap temp = Bitmap.createScaledBitmap(bitmap, imageWidth,imageHeight, true);
                contactImage.setImageBitmap(GenericUtils.getCroppedBitmap(temp));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.with(getContext()).load(contact.getImageURL()).into(imageTarget);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
