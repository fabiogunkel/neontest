package com.example.fabio.neontest.ui.sendmoney;


import android.widget.EditText;

import com.example.fabio.neontest.models.Contact;
import com.example.fabio.neontest.models.Transaction;

import java.util.List;

/**
 * Created by EUROCOM on 19/08/2017.
 */

public interface SendMoneyContract {
    public interface View{

        public void showList(List<Contact> contacts);

        public void showPaymentOverlay(Contact contact);

        public void closePaymentOverlay();

        public void onContactClicked(Contact contact);

        public void sendMoneyToContactButtonClicked(Contact contact, EditText editText);

        public void makeSuccessDialogBox();

        public void makeFailureDialogBox();

        public void showLoadingDialog();

        public void hideLoadingDialog();

        void makeFailureToast(int invalidValue);
    }

    public interface Actions{

        public void onContactClicked(Contact contact);

        public void sendMoney(Transaction transaction);

    }
}
