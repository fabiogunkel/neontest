
package com.example.fabio.neontest.models;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

import rx.Observable;

public class Contact implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ClienteId")
    @Expose
    private String clienteId;
    @SerializedName("Valor")
    @Expose
    private Double valor;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("Data")
    @Expose
    private String data;

    private  String name;

    private String number;
    private String imageURL;
    private String email;
    public Bitmap image;

    public Contact(String name, String number, String id, String imageURL) {
        this.name = name;
        this.number = number;
        this.clienteId = id;
        this.imageURL = imageURL;

    }
    public Contact(String name, String number, String id, String email, String imageURL) {
        this.name = name;
        this.number = number;
        this.clienteId = id;
        this.imageURL = imageURL;
        this.email = email;

    }

    protected Contact(Parcel in) {
        token = in.readString();
        data = in.readString();
        name = in.readString();
        number = in.readString();
        imageURL = in.readString();
        email = in.readString();
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClienteId() {
        return clienteId;
    }

    public void setClienteId(String clienteId) {
        this.clienteId = clienteId;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(data);
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(imageURL);
        dest.writeString(email);
        dest.writeString(clienteId);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void increaseValor(double increment) {
        valor += increment;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public static class IdComparator implements Comparator<Contact> {

        @Override
        public int compare(Contact contact1, Contact contact2) {
            return contact1.getClienteId().compareToIgnoreCase(contact2.getClienteId());
        }
    }

    public static class ValueComparator implements Comparator<Contact> {

        @Override
        public int compare(Contact contact1, Contact contact2) {
            return contact2.getValor().compareTo(contact1.getValor());
        }
    }
}