package com.example.fabio.neontest.adapters;

import com.example.fabio.neontest.models.Contact;

/**
 * Created by EUROCOM on 20/08/2017.
 */

public interface FriendsRecyclerAdapterContract {

    public interface View{
        public void onContactClicked(Contact contact);
    }
}
