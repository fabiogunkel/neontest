package com.example.fabio.neontest.apis;

import com.example.fabio.neontest.models.Contact;

import java.util.List;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public interface IContactsApi {
    public List<Contact> getContacts();
    public Contact getMyContact();
}
