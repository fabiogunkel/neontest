package com.example.fabio.neontest.factories;

import com.example.fabio.neontest.apis.IRequestsApi;
import com.example.fabio.neontest.apis.RequestsApi;
import com.example.fabio.neontest.ui.main.MainContract;
import com.example.fabio.neontest.ui.main.MainPresenter;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public class RequestsFactory {
    public static IRequestsApi getRequestsApi() {
        return new RequestsApi();
    }
}
