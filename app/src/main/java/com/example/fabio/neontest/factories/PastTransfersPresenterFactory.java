package com.example.fabio.neontest.factories;

import com.example.fabio.neontest.ui.pasttransfers.PastTransfersContract;
import com.example.fabio.neontest.ui.pasttransfers.PastTransfersPresenter;
import com.example.fabio.neontest.ui.sendmoney.SendMoneyContract;
import com.example.fabio.neontest.ui.sendmoney.SendMoneyPresenter;

/**
 * Created by EUROCOM on 19/08/2017.
 */

public class PastTransfersPresenterFactory {
    public static PastTransfersContract.Actions getPastTransfersPresenter(PastTransfersContract.View view) {
        return new PastTransfersPresenter(view);
    }
}
