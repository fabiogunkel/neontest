package com.example.fabio.neontest.ui.pasttransfers;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.fabio.neontest.R;
import com.example.fabio.neontest.utils.MyAxisRender;
import com.example.fabio.neontest.adapters.FriendsRecyclerAdapter;
import com.example.fabio.neontest.adapters.FriendsRecyclerAdapterContract;
import com.example.fabio.neontest.factories.PastTransfersPresenterFactory;
import com.example.fabio.neontest.models.Contact;
import com.example.fabio.neontest.ui.BaseActivity;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

public class PastTransfersActivity extends BaseActivity implements PastTransfersContract.View, FriendsRecyclerAdapterContract.View{
    private PastTransfersContract.Actions mPresenter;
    private RecyclerView rvContacts;
    private FriendsRecyclerAdapter adapter;
    private int imagesLoaded;
    private List<Target> imageTargets;
    private BarChart gvGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_transfers);

        imagesLoaded = 0;
        rvContacts = (RecyclerView) findViewById(R.id.rvContacts);
        rvContacts.setHasFixedSize(true);
        rvContacts.setLayoutManager(new LinearLayoutManager(this));

        gvGraph = (BarChart) findViewById(R.id.graph);

        mPresenter = PastTransfersPresenterFactory.getPastTransfersPresenter(this);
        mPresenter.getContactsTransactions();

    }

    @Override
    public int getActivityLabel() {
        return R.string.past_transfers_activity;
    }

    @Override
    public void showContactList(final List<Contact> contacts, List<Contact> accumulatedContacts) {
        adapter = new FriendsRecyclerAdapter(this, accumulatedContacts, this);
        rvContacts.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        imageTargets = new ArrayList<>();
        for(int i=0;i<contacts.size(); i++){
            final int finalI = i;
            imageTargets.add(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    contacts.get(finalI).setImage(bitmap);
                    imagesLoaded++;
                    if(imagesLoaded>=contacts.size()){
                        hideLoadingDialog();
                        makeGraph(contacts);
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    hideLoadingDialog();
                    makeFailureDialogBox();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
            Picasso.with(this).load(contacts.get(i).getImageURL()).into(imageTargets.get(i));
        }

    }

    private void makeGraph(final List<Contact> contacts){

        final List<String> labels = new ArrayList<>();
        List<BarEntry> data = new ArrayList<>();
        for (int i=0; i<contacts.size(); i++) {
            BarEntry barEntry = new BarEntry(i,contacts.get(i).getValor().floatValue());
            labels.add(contacts.get(i).getName());
            data.add(barEntry);
        }




        BarDataSet dataSet = new BarDataSet(data,""); // add entries to dataset

        dataSet.setColor(getColor(R.color.graphDataColor));
        dataSet.setValueTextColor(getColor(R.color.graphDataColor));

        BarData barData = new BarData(dataSet);
        barData.setValueTextSize(20);

        barData.setBarWidth(0.03f);
        gvGraph.setData(barData);
        gvGraph.getXAxis().setEnabled(true);
        gvGraph.getXAxis().setDrawAxisLine(false);
        gvGraph.getXAxis().setDrawGridLines(false);
        gvGraph.setXAxisRenderer(new MyAxisRender(gvGraph.getViewPortHandler(),gvGraph.getXAxis(), gvGraph.getTransformer(YAxis.AxisDependency.LEFT) , contacts, this));
//        gvGraph.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return labels.get((int)value);
//            }
//
//        });
        gvGraph.getXAxis().setTextColor(getColor(R.color.graphDataColor));
        gvGraph.getXAxis().setTextSize(10);
        gvGraph.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        gvGraph.getAxisLeft().setEnabled(false);
        gvGraph.getAxisRight().setEnabled(false);
        gvGraph.setVisibleXRange(0,4);
        gvGraph.getXAxis().setLabelCount((int)gvGraph.getVisibleXRange());
        gvGraph.animateY(2000, Easing.EasingOption.EaseInBack);
        gvGraph.setScaleEnabled(false);
        gvGraph.getDescription().setEnabled(false);
        gvGraph.getLegend().setEnabled(false);
        gvGraph.getAxis(YAxis.AxisDependency.LEFT).setAxisMinimum(-230);
        //gvGraph.setRenderer(new CustomBarChartRenderer(gvGraph, gvGraph.getAnimator(), gvGraph.getViewPortHandler(),contacts,this));
    }

    @Override
    public void onContactClicked(Contact contact) {

    }



}
