package com.example.fabio.neontest.apis;

import com.example.fabio.neontest.models.Contact;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public class RequestsApi implements IRequestsApi {

    private static Gson gson;
    private static EndPoints endPoints;
    public static Retrofit retrofit = null;
    public static final String BASE_URL = "http://processoseletivoneon.azurewebsites.net/";

    @Override
    public Observable<String> getToken(String name, String email) {
        return endPoints.generateToken(name, email);
    }

    @Override
    public Observable<String> sendMoney(String clientID, String token, double value) {
        return endPoints.sendMoney(clientID,token,value);
    }

    @Override
    public Observable<ArrayList<Contact>> getTransfers(String token) {
        return endPoints.getTransfers(token);
    }

    public RequestsApi()
    {
        if(gson == null) {

            RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(rxAdapter)
                    .build();
        }
        if(endPoints == null) {
            endPoints = retrofit.create(EndPoints.class);
        }
    }

    public interface EndPoints {
        // Request method and URL specified in the annotation
        // Callback for the parsed response is the last parameter

        @GET("GenerateToken")
        Observable<String> generateToken(@Query("nome") String username, @Query("email") String email);
        @FormUrlEncoded
        @POST("SendMoney")
        Observable<String> sendMoney(@Field("ClienteId") String id, @Field("token") String token, @Field("valor") double value);
        @GET("getTransfers")
        Observable<ArrayList<Contact>> getTransfers(@Query("token") String token);


    }
}
