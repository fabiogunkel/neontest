package com.example.fabio.neontest.ui;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.example.fabio.neontest.R;
import com.example.fabio.neontest.ui.main.MainActivity;

/**
 * Created by EUROCOM on 20/08/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toolbar actionbar = (Toolbar) findViewById(R.id.my_toolbar);
        if(actionbar!=null) {
            ((Button) actionbar.findViewById(R.id.btBack)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            TextView activityLabel = (TextView) findViewById(R.id.tvScreenName);
            activityLabel.setText(getActivityLabel());
        }


    }

    public void makeFailureDialogBox() {
        runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = new Dialog(ctx);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.ok_dialog);

                TextView text = (TextView) dialog.findViewById(R.id.tvDescription);
                text.setText(getString(R.string.operationFailed));

                Button dialogButton = (Button) dialog.findViewById(R.id.btOK);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        finish();
                    }
                });

                dialog.show();
            }
        });
    }
    public void makeSuccessDialogBox() {
        runOnUiThread(new Runnable() {
            public void run() {
            final Dialog dialog = new Dialog(ctx);
            dialog.setContentView(R.layout.ok_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView text = (TextView) dialog.findViewById(R.id.tvDescription);
            text.setText(getString(R.string.operationSucceeded));

            Button dialogButton = (Button) dialog.findViewById(R.id.btOK);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            }
        });
    }

    public void hideLoadingDialog() {
        runOnUiThread(new Runnable() {
            public void run() {
            if(dialog.isShowing())
            {
                dialog.hide();
            }
            }
        });
    }

    public void showLoadingDialog() {
        runOnUiThread(new Runnable() {
            public void run() {
            if(dialog == null) {
                dialog = new ProgressDialog(ctx);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.setIndeterminate(true);
                dialog.setCanceledOnTouchOutside(false);
            }
            if(!dialog.isShowing()) {
                dialog.show();
            }
            }
        });
    }


    public void makeFailureToast(int messageId) {
        Toast.makeText(this,getText(messageId),Toast.LENGTH_LONG).show();
    }

    public abstract int getActivityLabel();
}
