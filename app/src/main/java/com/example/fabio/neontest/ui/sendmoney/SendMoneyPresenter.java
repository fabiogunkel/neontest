package com.example.fabio.neontest.ui.sendmoney;

import android.app.Dialog;
import android.util.Log;

import com.example.fabio.neontest.NeonTestApplication;
import com.example.fabio.neontest.R;
import com.example.fabio.neontest.apis.IRequestsApi;
import com.example.fabio.neontest.factories.RequestsFactory;
import com.example.fabio.neontest.models.Contact;
import com.example.fabio.neontest.apis.IContactsApi;
import com.example.fabio.neontest.factories.ContactFactory;
import com.example.fabio.neontest.models.Transaction;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.functions.Action1;


/**
 * Created by EUROCOM on 19/08/2017.
 */

public class SendMoneyPresenter implements SendMoneyContract.Actions {
    SendMoneyContract.View view;
    IContactsApi mContactsApi;
    IRequestsApi mRequestsApi;

    public SendMoneyPresenter(SendMoneyContract.View view) {
        this.view = view;
        mContactsApi = ContactFactory.getContactApi();
        mRequestsApi = RequestsFactory.getRequestsApi();
        view.showList(mContactsApi.getContacts());
    }

    @Override
    public void sendMoney(Transaction transaction) {
        if(transaction.getValue()<=0){
            view.makeFailureToast(R.string.invalidValue);
            return;
        }

        view.showLoadingDialog();
        mRequestsApi
            .sendMoney(transaction.getContact().getClienteId(), NeonTestApplication.token, transaction.getValue())
            .subscribe(
                new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideLoadingDialog();
                        Log.e("Request Error:",e.getLocalizedMessage());
                        if (e instanceof HttpException) {
                            view.makeFailureDialogBox();
                        }
                        view.closePaymentOverlay();
                    }

                    @Override
                    public void onNext(String s) {
                        Log.w("SENT MONEY RESULT",s);
                        view.hideLoadingDialog();
                        view.closePaymentOverlay();
                        view.makeSuccessDialogBox();
                    }
                }
            );
    }

    @Override
    public void onContactClicked(Contact contact) {
        view.showPaymentOverlay(contact);
    }
}
