package com.example.fabio.neontest.factories;

import com.example.fabio.neontest.apis.ContactsApi;
import com.example.fabio.neontest.apis.IContactsApi;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public class ContactFactory {

    public static IContactsApi getContactApi() {
        return new ContactsApi();
    }
}
