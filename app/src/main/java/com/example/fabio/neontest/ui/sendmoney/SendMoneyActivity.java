package com.example.fabio.neontest.ui.sendmoney;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.example.fabio.neontest.adapters.FriendsRecyclerAdapter;
import com.example.fabio.neontest.adapters.FriendsRecyclerAdapterContract;
import com.example.fabio.neontest.models.Contact;
import com.example.fabio.neontest.R;
import com.example.fabio.neontest.utils.GenericUtils;
import com.example.fabio.neontest.factories.SendMoneyPresenterFactory;
import com.example.fabio.neontest.models.Transaction;
import com.example.fabio.neontest.ui.BaseActivity;
import com.squareup.picasso.Target;

import java.util.List;


public class SendMoneyActivity extends BaseActivity implements SendMoneyContract.View, FriendsRecyclerAdapterContract.View{
    private SendMoneyContract.Actions mPresenter;
    private RecyclerView rvContacts;
    private FriendsRecyclerAdapter adapter;
    private SendMoneyContactFragment contactFragment;
    private List<Target> targetImages;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        rvContacts = (RecyclerView) findViewById(R.id.rvContacts);
        rvContacts.setHasFixedSize(true);
        rvContacts.setLayoutManager(new LinearLayoutManager(this));
        mPresenter  = SendMoneyPresenterFactory.SendMoneyPresenter(this);
    }

    @Override
    public int getActivityLabel() {
        return R.string.send_money_activity;
    }

    @Override
    public void onBackPressed() {
        if(contactFragment==null || !contactFragment.isVisible()) {
            super.onBackPressed();
        }
        else {
            closePaymentOverlay();
        }

    }

    @Override
    public void showList(List<Contact> contacts) {
        adapter = new FriendsRecyclerAdapter(this, contacts, this);
        rvContacts.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void showPaymentOverlay(Contact contact) {
        contactFragment = new SendMoneyContactFragment();
        Bundle content = new Bundle();
        content.putParcelable("Contact", contact);
        contactFragment.setArguments(content);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.ffFragmentContainer, contactFragment).commit();
    }

    @Override
    public void closePaymentOverlay() {
        getSupportFragmentManager().beginTransaction().remove(contactFragment).commit();
        contactFragment = null;
    }

    @Override
    public void onContactClicked(Contact contact) {
        mPresenter.onContactClicked(contact);
    }

    @Override
    public void sendMoneyToContactButtonClicked(Contact contact, EditText editText) {
        Transaction transaction = new Transaction(contact, GenericUtils.getCleanValue(editText));
        mPresenter.sendMoney(transaction);
    }


}
