package com.example.fabio.neontest.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.fabio.neontest.utils.GenericUtils;
import com.example.fabio.neontest.models.Contact;
import com.example.fabio.neontest.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EUROCOM on 19/08/2017.
 */

public class FriendsRecyclerAdapter extends RecyclerView.Adapter<FriendsRecyclerAdapter.ViewHolder> {

    private List<Contact> contacts;
    FriendsRecyclerAdapterContract.View view;
    private  List<Target> targetImages;
    Context ctx;

    public FriendsRecyclerAdapter(FriendsRecyclerAdapterContract.View view, List<Contact> contacts, Context ctx) {
        this.view = view;
        this.contacts = contacts;
        this.ctx = ctx;
        targetImages = new ArrayList<>();
    }

    @Override
    public FriendsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from( parent.getContext() )
                .inflate(R.layout.contact_component, parent, false);
        FriendsRecyclerAdapter.ViewHolder viewHolder = new FriendsRecyclerAdapter.ViewHolder( view );
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Contact contact = contacts.get(position);
        holder.tvContactName.setText(contact.getName());
        holder.tvContactNumber.setText(contact.getNumber());
        holder.llContactComponent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.onContactClicked(contact);
            }
        });
        if(contact.getValor() != null)
        {
            holder.tvValueReceived.setText(NumberFormat.getCurrencyInstance().format(contact.getValor()));
            holder.tvValueReceived.setVisibility(View.VISIBLE);
        }

        final ImageView contactImage = holder.ivContactImage;
        final int imageWidth = contactImage.getLayoutParams().width;
        final int imageHeight = contactImage.getLayoutParams().height;
        targetImages.add(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, imageWidth, imageHeight, true);
                contactImage.setImageBitmap(GenericUtils.getCroppedBitmap(scaledBitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
        Picasso.with(ctx).load(contact.getImageURL()).into(targetImages.get(position));
    }

    public void setImage(int index, Bitmap croppedBitmap) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivContactImage;
        private TextView  tvContactName;
        private TextView  tvContactNumber;
        private TextView  tvValueReceived;
        private LinearLayout llContactComponent;

        public ViewHolder(View itemView) {
            super(itemView);

            ivContactImage = (ImageView) itemView.findViewById(R.id.ivContactView);
            tvContactName = (TextView) itemView.findViewById(R.id.tvContactName);
            tvContactNumber = (TextView) itemView.findViewById(R.id.tvContactNumber);
            tvValueReceived = (TextView) itemView.findViewById(R.id.tvValueReceived);
            llContactComponent = (LinearLayout) itemView.findViewById(R.id.llContactComponent);

        }
    }
}
