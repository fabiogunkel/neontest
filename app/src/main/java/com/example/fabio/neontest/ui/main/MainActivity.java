package com.example.fabio.neontest.ui.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fabio.neontest.utils.GenericUtils;
import com.example.fabio.neontest.models.Contact;
import com.example.fabio.neontest.R;
import com.example.fabio.neontest.factories.MainPresenterFactory;
import com.example.fabio.neontest.ui.BaseActivity;
import com.example.fabio.neontest.ui.pasttransfers.PastTransfersActivity;
import com.example.fabio.neontest.ui.sendmoney.SendMoneyActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MainActivity extends BaseActivity implements MainContract.View {

    private MainContract.Actions mPresenter;
    private ImageView ivImageView;
    private TextView  tvUserName;
    private TextView  tvUserEmail;
    private Button    btSendMoney;
    private Button    btSeePastTransactions;
    private Target    target;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = MainPresenterFactory.getMainPresenter(this);
        loadView();
    }

    @Override
    public int getActivityLabel() {
        return R.string.app_name;
    }

    private void loadView() {

        btSendMoney = (Button) findViewById(R.id.btSendMoney);
        btSeePastTransactions = (Button) findViewById(R.id.btSeePastTransactions);
        btSendMoney.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openSendMoneyActivity();
            }
        });

        btSeePastTransactions.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPastTransfersActivity();
            }
        });
    }

    @Override
    public void showUser(Contact contact) {
        ivImageView = (ImageView) findViewById(R.id.ivUserImage);
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserEmail = (TextView) findViewById(R.id.tvUserEmail);

        tvUserName.setText(contact.getName());
        tvUserEmail.setText(contact.getEmail());

        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                ivImageView.setImageBitmap(GenericUtils.getCroppedBitmap(bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                makeFailureDialogBox();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        GenericUtils.processImageURL(contact.getImageURL(),this,target);
    }

    @Override
    public void openSendMoneyActivity() {
        Intent intent = new Intent(this, SendMoneyActivity.class);
        startActivity(intent);
    }

    @Override
    public void openPastTransfersActivity() {
        Intent intent = new Intent(this, PastTransfersActivity.class);
        startActivity(intent);
    }

}
