package com.example.fabio.neontest.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.fabio.neontest.models.Contact;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

public class MyAxisRender extends XAxisRenderer {

    List<Contact> contacts;
    List<Target> imageTargets;
    private Context ctx;

    public MyAxisRender(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans, List<Contact> contacts, Context ctx) {
        super(viewPortHandler, xAxis, trans);
        this.contacts = contacts;
        this.ctx = ctx;
        imageTargets = new ArrayList<>();
    }

    @Override
    protected void drawLabels(final Canvas c, final float pos, final MPPointF anchor){
        super.drawLabels(c, pos, anchor);

        final float labelRotationAngleDegrees = mXAxis.getLabelRotationAngle();
        boolean centeringEnabled = mXAxis.isCenterAxisLabelsEnabled();

        float[] positions = new float[mXAxis.mEntryCount * 2];

        for (int i = 0; i < positions.length; i += 2) {

            // only fill x values
            if (centeringEnabled) {
                positions[i] = mXAxis.mCenteredEntries[i / 2];
            } else {
                positions[i] = mXAxis.mEntries[i / 2];
            }
        }

        mTrans.pointValuesToPixel(positions);

        for (int i = 0; i < positions.length; i += 2) {

            float x = positions[i];
            if (mViewPortHandler.isInBoundsX(x)) {

                Bitmap image = GenericUtils.getResizedBitmap(contacts.get((int)mXAxis.mEntries[i / 2]).getImage(),100, 100);
                if (mXAxis.isAvoidFirstLastClippingEnabled()) {

                    // avoid clipping of the last
                    if (i == mXAxis.mEntryCount - 1 && mXAxis.mEntryCount > 1) {
                        float width = image.getWidth();

                        if (width > mViewPortHandler.offsetRight() * 2
                                && x + width > mViewPortHandler.getChartWidth())
                            x -= width / 2;

                        // avoid clipping of the first
                    } else if (i == 0) {

                        float width = image.getWidth();
                        x += width / 2;
                    }
                }
                drawLabel(c, GenericUtils.getCroppedBitmap(image), x-image.getWidth()/2, pos-image.getHeight(), anchor, labelRotationAngleDegrees);
            }
        }
    }
    private static Rect mDrawTextRectBuffer = new Rect();
    private static Paint.FontMetrics mFontMetricsBuffer = new Paint.FontMetrics();
    protected void drawLabel(Canvas c, Bitmap formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
        float drawOffsetX = 0.f;
        float drawOffsetY = 0.f;

        // Android sometimes has pre-padding
        drawOffsetX -= mDrawTextRectBuffer.left;

        // Android does not snap the bounds to line boundaries,
        //  and draws from bottom to top.
        // And we want to normalize it.
        drawOffsetY += -mFontMetricsBuffer.ascent;

        // To have a consistent point of reference, we always draw left-aligned
        Paint.Align originalTextAlign = mAxisLabelPaint.getTextAlign();
        mAxisLabelPaint.setTextAlign(Paint.Align.LEFT);

        if (angleDegrees != 0.f) {

            // Move the text drawing rect in a way that it always rotates around its center
            drawOffsetX -= mDrawTextRectBuffer.width() * 0.5f;

            float translateX = x;
            float translateY = y;


            c.save();
            c.translate(translateX, translateY);
            c.rotate(angleDegrees);

            c.drawBitmap(formattedLabel, drawOffsetX, drawOffsetY, mAxisLabelPaint);

            c.restore();
        } else {

            drawOffsetX += x;
            drawOffsetY += y;

            c.drawBitmap(formattedLabel, drawOffsetX, drawOffsetY, mAxisLabelPaint);
        }

        mAxisLabelPaint.setTextAlign(originalTextAlign);

    }

    @Override
    protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
        //Utils.drawXAxisValue(c, formattedLabel, x, y, mAxisLabelPaint, anchor, angleDegrees);
    }
}