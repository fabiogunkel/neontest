package com.example.fabio.neontest.factories;

import com.example.fabio.neontest.apis.ContactsApi;
import com.example.fabio.neontest.apis.IContactsApi;
import com.example.fabio.neontest.ui.main.MainContract;
import com.example.fabio.neontest.ui.main.MainPresenter;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public class MainPresenterFactory {
    public static MainContract.Actions getMainPresenter(MainContract.View view) {
        return new MainPresenter(view);
    }
}
