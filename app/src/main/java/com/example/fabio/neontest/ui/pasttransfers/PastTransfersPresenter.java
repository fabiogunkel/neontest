package com.example.fabio.neontest.ui.pasttransfers;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.example.fabio.neontest.NeonTestApplication;
import com.example.fabio.neontest.apis.ContactsApi;
import com.example.fabio.neontest.apis.IContactsApi;
import com.example.fabio.neontest.apis.IRequestsApi;
import com.example.fabio.neontest.apis.RequestsApi;
import com.example.fabio.neontest.factories.ContactFactory;
import com.example.fabio.neontest.factories.RequestsFactory;
import com.example.fabio.neontest.models.Contact;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by EUROCOM on 20/08/2017.
 */

public class PastTransfersPresenter implements PastTransfersContract.Actions {
    PastTransfersContract.View view;
    IRequestsApi mRequestsApi;
    private  List<Target> targetImages;

    public PastTransfersPresenter(PastTransfersContract.View view) {
        this.view = view;
        mRequestsApi = RequestsFactory.getRequestsApi();
        targetImages = new ArrayList<>();
    }

    @Override
    public void getContactsTransactions() {
        view.showLoadingDialog();
        mRequestsApi
        .getTransfers(NeonTestApplication.token)
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
        .unsubscribeOn(Schedulers.io())
        .subscribe(
            new Subscriber<ArrayList<Contact>>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    Log.e("Request error:", e.getLocalizedMessage());
                    view.hideLoadingDialog();
                    view.makeFailureDialogBox();
                }

                @Override
                public void onNext(ArrayList<Contact> contacts) {
                    processContacts(contacts);
                }
            }
        );
    }

    private void processContacts(List<Contact> contacts) {
        int i =0;
        List<Contact> savedContacts = ContactFactory.getContactApi().getContacts();
        for (i=0; i < contacts.size(); i++){
            for (Contact myContact: savedContacts) {
                if(contacts.get(i).getClienteId().equals(myContact.getClienteId())){
                    contacts.get(i).setName(myContact.getName());
                    contacts.get(i).setNumber(myContact.getNumber());
                    contacts.get(i).setImageURL(myContact.getImageURL());
                    break;
                }
            }
        }
        i=0;
        List<Contact> accumulatedContacts = new ArrayList<>(contacts);
        Collections.sort(contacts, new Contact.IdComparator());

        while(i < (contacts.size()-1))
        {
            if(contacts.get(i).getClienteId().equals(contacts.get(i+1).getClienteId())){
                contacts.get(i).increaseValor(contacts.get(i+1).getValor());
                contacts.remove(i+1);
            }
            else {
                i++;
            }
        }
        Collections.sort(contacts,new Contact.ValueComparator());

        view.showContactList(contacts, accumulatedContacts);
    }
}
