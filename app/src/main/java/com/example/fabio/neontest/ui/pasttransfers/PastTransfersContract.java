package com.example.fabio.neontest.ui.pasttransfers;

import com.example.fabio.neontest.models.Contact;

import java.util.List;

/**
 * Created by EUROCOM on 20/08/2017.
 */

public interface PastTransfersContract {

    public interface View {
        public void showContactList(List<Contact> contacts, List<Contact> accumulatedContacts);

        public void makeFailureDialogBox();

        public void showLoadingDialog();
        public void hideLoadingDialog();
    }

    public interface Actions {
        public void getContactsTransactions();

    }
}
