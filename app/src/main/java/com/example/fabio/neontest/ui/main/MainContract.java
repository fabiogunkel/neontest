package com.example.fabio.neontest.ui.main;

import com.example.fabio.neontest.models.Contact;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public interface MainContract {

    public interface View {
        public void showUser(Contact contact);

        public void openSendMoneyActivity();

        public void openPastTransfersActivity();

        public void makeFailureDialogBox();

        public void hideLoadingDialog();

        public void showLoadingDialog();
    }

    public interface Actions {
        public void getToken();
    }
}
