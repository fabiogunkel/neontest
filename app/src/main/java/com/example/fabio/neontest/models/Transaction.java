package com.example.fabio.neontest.models;

/**
 * Created by EUROCOM on 20/08/2017.
 */

public class Transaction {

    private Contact contact;
    private Double value;

    public Transaction(Contact contact, double value) {
        this.contact = contact;
        this.value = value;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
