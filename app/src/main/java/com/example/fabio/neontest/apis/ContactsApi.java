package com.example.fabio.neontest.apis;

import com.example.fabio.neontest.models.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EUROCOM on 18/08/2017.
 */

public class ContactsApi implements IContactsApi{

    @Override
    public List<Contact> getContacts() {
        List<Contact> myContacts = new ArrayList<>();
        myContacts.add(new Contact("Maria da Gloria","(99)99999-9901", "1", "http://cdn-ugc.mamaslatinas.com/gen/constrain/500/500/80/2012/08/31/13/c8/hd/po9kjc0nsc5ge.jpg"));
        myContacts.add(new Contact("Cristiane ","(99)99999-9902", "2", "http://www.empowher.com/sites/default/files/herarticle/overlooked-happy-person.jpg"));
        myContacts.add(new Contact("Mari","(99)99999-9903", "3", "http://www.pick-health.com/wp-content/uploads/2013/08/happy-person.jpg"));
        myContacts.add(new Contact("João","(99)99999-9904", "4", "http://brightdrops.com/wp-content/uploads/2014/06/happinessquotes.jpg"));
        myContacts.add(new Contact("Joana","(99)99999-9905", "5", "https://i.elitestatic.com/content/uploads/2017/05/08022041/happy-woman-smiling-eyes-closed.jpg"));
        myContacts.add(new Contact("Pedro","(99)99999-9906", "6", "http://projectlifemastery.com/wp-content/uploads/2013/11/positive-people.jpg"));
        myContacts.add(new Contact("Patrícia","(99)99999-9907", "7", "http://psikologid.com/wp-content/uploads/2014/05/happy-10.jpg"));
        myContacts.add(new Contact("Osmar","(99)99999-9908", "8", "https://img.washingtonpost.com/wp-apps/imrs.php?src=https://img.washingtonpost.com/rf/image_908w/2010-2019/WashingtonPost/2015/03/27/Production/Sunday/SunBiz/Images/Laszlo%20WR%20Headshot%20HI.jpg&w=480"));
        myContacts.add(new Contact("Ricardo","(99)99999-9909", "9", "http://4.bp.blogspot.com/_cQa_T_famDY/THQJPxdqhYI/AAAAAAAABxM/V9EHBJJmEu8/s800/Moods_of_Norway-man.jpg"));
        myContacts.add(new Contact("Carol","(99)99999-9910", "10", "https://i3.ypcdn.com/blob/3fd70de4c4e062c47343661568674a82a660666c"));
        myContacts.add(new Contact("Josimar","(99)99999-9911", "11", "https://i.pinimg.com/736x/73/27/e7/7327e74211c0eac6e87facfe48fcb147--rd-birthday-happy-birthday.jpg"));
        myContacts.add(new Contact("Natália","(99)99999-9912", "12", "https://timedotcom.files.wordpress.com/2016/03/joy-cho1.jpg?h=425"));
        myContacts.add(new Contact("Felipe","(99)99999-9913", "13", "http://zh.rbsdirect.com.br/imagesrc/23242571.jpg?w=640"));
        myContacts.add(new Contact("Helô","(99)99999-9914", "14", "https://images1.minhavida.com.br/imagensConteudo/10771/Felicidade.jpg"));
        myContacts.add(new Contact("Isabela","(99)99999-9915", "15", "http://images.huffingtonpost.com/2016-05-27-1464381878-6932997-11HabitsofSupremelyHappyPeopleHP.jpg"));
        return myContacts;
    }

    @Override
    public Contact getMyContact() {
        Contact contact = new Contact("Fabio Gunkel","(99)99999-9999", "fabiogunkel", "fabiogunkel@gmail.com", "https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAgmAAAAJGQwZjZiMzc5LWZkN2MtNDIxYi1hMjA2LWIyYzE0OTlmMWIyNw.jpg");
        return contact;
    }

}
